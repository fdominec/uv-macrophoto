#!/bin/bash

## the alphabetical ordering prefix is no more needed
rename s/[abcdefgh].[xy]_//g */

for sample in vz*/
do 
	echo
	echo
	echo
	echo
	echo `date` PROCESSING $sample

	## Usual: If processing multiple TIFs in directories
	pushd $sample
	mogrify -format ppm *tif # prepare eight or so PPMs, since the next "ale" command does not accept TIFs
	ale-bin *ppm out.ppm
	convert out.ppm  -unsharp 3x.5+1+0 ../${sample%/}-full.tif
	rm *.ppm
	popd


	## Extra: If processing already aligned (or single) TIF
	#sample=${sample%-full.tif}

	convert ${sample%/}-full.tif  \
	    -fill black -draw "rectangle 0 0  1100 100" -draw "rectangle 2600 0  9999 100" -draw "rectangle 3000 0  9999 200" \
	    -colorspace HSV -channel L -contrast-stretch .3% -unsharp 3x.5+1+0 ${sample%/}-full.jpg
	convert ${sample%/}-full.tif -resize 50% \
	    -fill black -draw "rectangle 0 0  550 50" -draw "rectangle 1300 0  9999 50" -draw "rectangle 1500 0  9999 100" \
	    -colorspace HSV -channel L -contrast-stretch .3% -unsharp 3x.5+1+0 ${sample%/}-medium.jpg

	composite -compose Multiply \( -resize `identify -format '%wx%h' ${sample%/}-medium.jpg` \) ../../grid.png  ${sample%/}-medium.jpg  ${sample%/}-medium-grid.jpg

done

	#convert out.ppm  -unsharp 3x.5+1+0             -contrast-stretch 1% -fx 'u*0.9' ../${sample%/}-full.jpg
	#convert out.ppm  -unsharp 3x.5+1+0 -resize 50% -contrast-stretch 1% -fx 'u*0.9' ../${sample%/}-medium.tif
	#convert out.ppm  -unsharp 3x.5+1+0 -resize 50% -contrast-stretch 1% -fx 'u*0.9' ../${sample%/}-medium.jpg
	#convert out.ppm  -unsharp 3x.5+1+0 -resize 25% -contrast-stretch 0% ../${sample%/}-small.jpg
	# new processing:
	#for x in vz*tif; do 
	#convert out.ppm  -resize 50% -fill black -draw "rectangle 0 0  550 50" -draw "rectangle 1300 0  9999 50" -draw "rectangle 1500 0  9999 100"
	#   -colorspace HSV -channel L -contrast-stretch .3% -unsharp 3x.5+1+0 $x-NEWmedium-reg.jpg; done
	# for f in *-medium.jpg; do composite -compose Multiply \( -resize `identify -format '%wx%h' ${f}` \) ../../grid.png  ${f}  ../${f%.jpg}-grid.jpg; done 
