
##  Makrofotky 2" waferů - instalace 

Potřebné soubory ke zkopírování:
	grid.png  makedirs.py  process_converted.sh  rawtherapee-profil-opraveny.pp3  README-UVFoto.md

Na Ubuntu je potřeba:
	sudo apt-get install -y libimage-exiftool-perl rawtherapee ale

Manuální nastavení Rawtherapee: 
	/Queue\ --> File Format --> TIFF(16bit), [X] uncompressed TIFF, [ ] save processing param

##  Makrofotky 2" waferů - postup 


 # na foťáku by mělo být nastaveno:    mode="Manual"   shutter=1.0    quality=RAW   (a zkotrolovat, že objektiv je na cloně 5.6 a že je zaostřeno)
 # nafoť a stáhni všechny *.ARW  do složky v počítači, pak v této složce spusť

exiftool -a -G1 -orientation=Vertical -cameraorientation=Vertical -rotation=Vertical -overwrite_original *.ARW ; rm -r ~/.cache/RawTherapee/images ; rawtherapee . 

 # doporučeno: dvojklik na první z fotek a načíst (vpravo nahoře) profil rawtherapee-profil-opraveny.pp3
 #   * ořezový rámec bude asi potřebovat ručně posunout podle osy y, aby přesně seděl na kraje waferu (shift+myš), 
 #   * zkopírovat tuto změnu na ostatní -> ctrl+C, ctrl+A, ctrl+V
 #   * spustit konverzi do 16-bit TIFF pomocí ctrl+A, ctrl+B

 # vytvoř složky podle vzorků ve tvaru aax_vz080A, aay_vz080A_c403, abx_vz100B ... :

mkcd converted; python3 ../../makedirs.py 080a 100b

 # ručně do nich rozestrkat vytvořené TIF soubory např. v "geeqie" a odstranit předpony:
 # zpracovat TIFFy trvá několik minut na vzorek a hodně to vytíží počítač: 

rename s/[abcdefgh].[xy]_//g */
~/f/process_converted.sh
geeqie &

 # je-li vše OK, uklidit po sobě a nahrát na limbu: 
 
mv ../*ARW* ../../tmp-arw-pred-konverzi
mv vz*/ ../../tmp-tiff-pred-registraci
cp *medium.jpg ~/LIMBA/Nitridy\ mereni/UV-makrofotografie/mid-res2k/ -rvn 
cp *medium-grid.jpg ~/LIMBA/Nitridy\ mereni/UV-makrofotografie/mid-res2k-grid/ -rvn 
cp *full.jpg ~/LIMBA/Nitridy\ mereni/UV-makrofotografie/full-res4k/ -rvn 
cp *full.tif ~/LIMBA/Nitridy\ mereni/UV-makrofotografie/tiff-full-res4k/ -rvn 

Anebo čtyřpalce:

cp *medium.jpg ~/LIMBA/Nitridy\ mereni/UV-makrofotografie/4inch_mid-res2k/ -v
