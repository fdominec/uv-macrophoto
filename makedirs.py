#!/usr/bin/python3  
#-*- coding: utf-8 -*-

## Import common moduli
import sys,os
prefixes = ['aax_', 'aay_', 'abx_', 'aby_', 'acx_', 'acy_', 'adx_', 'ady_', 'aex_', 'aey_', 'afx_', 
		'afy_', 'agx_', 'agy_', 'ahx_', 'ahy_', 'aix_', 'aiy_', 'ajx_', 'ajy_', 'akx_', 
		'aky_', 'alx_', 'aly_', 'amx_', 'amy_', 'anx_', 'any_', 'aox_', 'aoy_', 'apx_', 
		'apy_', 'aqx_', 'aqy_', 'arx_', 'ary_', 'asx_', 'asy_', 'atx_', 'aty_', 'aux_', 
		'auy_', 'avx_', 'avy_', 'awx_', 'awy_', 'axx_', 'axy_', 'ayx_', 'ayy_', 'azx_', 
		'bax_', 'bay_', 'bbx_', 'bby_', 'bcx_', 'bcy_', 'bdx_', 'bdy_', 'bex_', 'bey_', 
		'bfx_', 'bfy_', 'bgx_', 'bgy_', 'bhx_', 'bhy_', 'bix_', 'biy_', 'bjx_', 'bjy_', 
		'bkx_', 'bky_', 'blx_', 'bly_', 'bmx_', 'bmy_', 'bnx_', 'bny_', 'box_', 'boy_', 
		'bpx_', 'bpy_', 'bqx_', 'bqy_', 'brx_', 'bry_', 'bsx_', 'bsy_', 'btx_', 'bty_', 
		'bux_', 'buy_', 'bvx_', 'bvy_', 'bwx_', 'bwy_', 'bxx_', 'bxy_', 'byx_', 'byy_', 'bzx_']


if len(sys.argv) > len(prefixes)/2: 
    print('too many parameters'); quit()
for p1,p2,x in zip(prefixes[:len(sys.argv)*2-1:2], prefixes[1:len(sys.argv)*2:2], sys.argv[1:]):
    os.mkdir(p1+'vz'+x.upper())
    # os.mkdir(p2+'vz'+x.upper()+'_c403') ## for pumping at different wavelength
    
